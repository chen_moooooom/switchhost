package com.chunqiu.mrjuly.modules.staff.service;

import javax.annotation.Resource;

import com.chunqiu.mrjuly.common.persistence.CrudService;
import org.springframework.stereotype.Service;
import com.chunqiu.mrjuly.modules.staff.model.Staff;
import com.chunqiu.mrjuly.modules.staff.dao.StaffDao;

/**
 * 职工管理Service
 * @author chen
 * @version 2019-12-02
 */
@Service
public class StaffService extends CrudService<StaffDao, Staff, Integer> {
	@Resource
	private StaffDao dao;

}