package com.chunqiu.mrjuly.modules.staff.model;

import com.chunqiu.mrjuly.common.utils.excel.annotation.ExcelField;
import org.hibernate.validator.constraints.Length;

import com.chunqiu.mrjuly.common.persistence.DataEntity;

/**
 * 职工管理Entity
 * @author chen
 * @version 2019-12-02
 */
public class Staff extends DataEntity<Staff, Integer> {
	
	private static final long serialVersionUID = 1L;
	@ExcelField(title = "职工姓名", type = 0, align = 2, sort = 0)
	private String name;		// 名称
	private String area;		// 所属区域
	
	public Staff() {
		super();
	}

	public Staff(Integer id){
		super(id);
	}

	@Length(min=0, max=255, message="名称长度必须介于 0 和 255 之间")
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	@Length(min=0, max=255, message="所属区域长度必须介于 0 和 255 之间")
	public String getArea() {
		return area;
	}

	public void setArea(String area) {
		this.area = area;
	}
	
}