package com.chunqiu.mrjuly.modules.project.service;

import javax.annotation.Resource;

import com.chunqiu.mrjuly.common.persistence.CrudService;
import org.springframework.stereotype.Service;
import com.chunqiu.mrjuly.modules.project.model.Project;
import com.chunqiu.mrjuly.modules.project.dao.ProjectDao;

/**
 * 项目管理Service
 * @author chen
 * @version 2019-12-03
 */
@Service
public class ProjectService extends CrudService<ProjectDao, Project, Integer> {
	@Resource
	private ProjectDao dao;

}