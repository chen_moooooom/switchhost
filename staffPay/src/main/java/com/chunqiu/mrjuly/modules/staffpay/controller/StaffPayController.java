package com.chunqiu.mrjuly.modules.staffpay.controller;

import com.chunqiu.mrjuly.common.annotation.Log;
import com.chunqiu.mrjuly.common.persistence.BaseController;
import com.chunqiu.mrjuly.common.utils.DateUtils;
import com.chunqiu.mrjuly.common.utils.excel.ExportExcel;
import com.chunqiu.mrjuly.common.vo.Grid;
import com.chunqiu.mrjuly.common.vo.GridParam;
import com.chunqiu.mrjuly.common.vo.Json;
import com.chunqiu.mrjuly.modules.staffpay.model.StaffPay;
import com.chunqiu.mrjuly.modules.staffpay.service.StaffPayService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * 职工薪资管理Controller
 * @author chen
 * @version 2019-12-03
 */
@Slf4j
@Controller
@RequestMapping(value = "${adminPath}/staffpay/staffPay")
public class StaffPayController extends BaseController {

	@Autowired
	private StaffPayService staffPayService;
	
	/**
	 * @Description：职工薪资管理列表页面
	 * @author chen
	 */
	@Log("打开职工薪资管理列表页面")
	@RequestMapping("index")
	public String index(){
		return "/staffpay/staffPayList";
	}
	/**
	 * @Description：职工薪资管理列表页面
	 * @author chen
	 */
	@Log("打开职工薪资管理列表页面")
	@RequestMapping("indexOfStaff")
	public String indexOfStaff(Integer id,ModelMap modelMap){
		modelMap.addAttribute("staffId",id);
		return "/staffpay/staffPayListOfStaff";
	}

	/**
	 * @Description：职工薪资管理列表数据
	 * @author chen
	 */
	@Log("获取职工薪资管理列表数据")
	@RequestMapping("list")
	@ResponseBody
	public Grid list(StaffPay staffPay, GridParam param) {
		return staffPayService.findPage(staffPay, param);
	}
	/**
	 * @Description：职工薪资管理表单页面--新增/编辑
	 * @author chen
	 */
	@Log("打开职工薪资管理表单页面--新增/编辑")
	@RequestMapping(value = "form")
	public String form(StaffPay staffPay, ModelMap model) {
		staffPay = staffPayService.get(staffPay.getId());
		model.addAttribute("staffPay", staffPay);
		return "/staffpay/staffPayForm";
	}
	/**
	 * @Description：职工薪资管理新增/编辑 保存方法
	 * @author chen
	 */
	@Log("新增/编辑职工薪资管理")
	@RequestMapping("saveStaffPay")
	public String saveStaffPay(StaffPay staffPay, ModelMap model){
		try{
			staffPayService.save(staffPay);
		}catch(Exception e){
			log.error("保存失败！ msg={}", e.getMessage(), e);

			model.addAttribute("error", "保存失败！");
			return form(staffPay, model);
		}
		return successPath;
	}
	/**
	 * @Description：职工薪资管理数据删除方法
	 * @author chen
	 */
	@Log("删除职工薪资管理")
	@RequestMapping("delStaffPay")
	@ResponseBody
	public Json delStaffPay(StaffPay staffPay){
		Json json = new Json();
		try{
			staffPayService.delete(staffPay);
			json.setSuccess(true);
		}catch(Exception e){
			log.error("删除失败！ msg={}", e.getMessage(), e);

			json.setSuccess(false);
			json.setMsg("删除失败！");
		}
		return json;
	}


	/**
	 * 导出数据
	 * @param staffPay
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "exportData", method = RequestMethod.GET)
	@ResponseBody
	public String exportData(StaffPay staffPay, HttpServletRequest request, HttpServletResponse response) {
		try {
			String fileName = "职工工资统计" + DateUtils.getDate("yyyyMMddHHmmss") + ".xlsx";
			List<StaffPay> list = staffPayService.findList(staffPay);
			new ExportExcel("职工工资统计", StaffPay.class).setDataList(list).write(response, fileName).dispose();
			return null;
		} catch (Exception e) {
			request.setAttribute("error", "导出数据失败");
		}
		return null;
	}

}