package com.chunqiu.mrjuly.modules.staffpay.dao;

import com.chunqiu.mrjuly.common.persistence.CrudDao;
import com.chunqiu.mrjuly.modules.staffpay.model.StaffPay;

/**
 * 职工薪资管理DAO接口
 * @author chen
 * @version 2019-12-03
 */
public interface StaffPayDao extends CrudDao<StaffPay, Integer> {
	
}