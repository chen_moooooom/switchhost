package com.chunqiu.mrjuly.modules.staff.dao;

import com.chunqiu.mrjuly.common.persistence.CrudDao;
import com.chunqiu.mrjuly.modules.staff.model.Staff;

/**
 * 职工管理DAO接口
 * @author chen
 * @version 2019-12-02
 */
public interface StaffDao extends CrudDao<Staff, Integer> {
	
}