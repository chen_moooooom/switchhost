package com.chunqiu.mrjuly.modules.staffpay.model;

import com.chunqiu.mrjuly.common.persistence.DataEntity;
import com.chunqiu.mrjuly.common.utils.excel.annotation.ExcelField;
import com.chunqiu.mrjuly.modules.project.model.Project;
import com.chunqiu.mrjuly.modules.staff.model.Staff;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import org.hibernate.validator.constraints.Length;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotEmpty;
import java.util.Date;

/**
 * 职工薪资管理Entity
 * @author chen
 * @version 2019-12-03
 */
@Data
public class StaffPay extends DataEntity<StaffPay, Integer> {
	
	private static final long serialVersionUID = 1L;
	private Integer staffId;
	private Integer projectId;
	private String natures;
	@ExcelField(title = "工资日期", type = 0, align = 2, sort = 3)
	@NotEmpty
	@DateTimeFormat(pattern = "yyyy-MM")
	private Date payDate;


	@ExcelField(title = "总工资", type = 0, align = 2, sort = 2)
	@NotEmpty
	private Double totalPrice;
	private Staff staff;
	private Project project;
	@ExcelField(title = "姓名", type = 0, align = 2, sort = 0)
	@NotEmpty
	private String name;
	@ExcelField(title = "项目", type = 0, align = 2, sort = 1)
	@NotEmpty
	private String projectName;



	@JsonFormat(pattern = "yyyy-MM")
	public Date getPayDate() {
		return payDate;
	}

	public void setPayDate(Date payDate) {
		this.payDate = payDate;
	}

	public StaffPay() {
		super();
	}

	public StaffPay(Integer id){
		super(id);
	}

	public Integer getStaffId() {
		return staffId;
	}

	public void setStaffId(Integer staffId) {
		this.staffId = staffId;
	}
	
	public Integer getProjectId() {
		return projectId;
	}

	public void setProjectId(Integer projectId) {
		this.projectId = projectId;
	}
	
	@Length(min=0, max=255, message="natures长度必须介于 0 和 255 之间")
	public String getNatures() {
		return natures;
	}

	public void setNatures(String natures) {
		this.natures = natures;
	}
	
}