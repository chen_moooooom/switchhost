package com.chunqiu.mrjuly.modules.project.controller;

import com.chunqiu.mrjuly.common.annotation.Log;
import com.chunqiu.mrjuly.common.persistence.BaseController;
import com.chunqiu.mrjuly.common.vo.Grid;
import com.chunqiu.mrjuly.common.vo.GridParam;
import com.chunqiu.mrjuly.common.vo.Json;
import com.chunqiu.mrjuly.modules.project.model.Project;
import com.chunqiu.mrjuly.modules.project.service.ProjectService;
import lombok.extern.slf4j.Slf4j;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

/**
 * 项目管理Controller
 * @author chen
 * @version 2019-12-03
 */
@Slf4j
@Controller
@RequestMapping(value = "${adminPath}/project/project")
public class ProjectController extends BaseController {

	@Autowired
	private ProjectService projectService;
	
	/**
	 * @Description：项目管理列表页面
	 * @author chen
	 */
	@Log("打开项目管理列表页面")
	@RequestMapping("index")
	@RequiresPermissions("project:project:view")
	public String index(){
		return "/project/projectList";
	}
	
	/**
	 * @Description：项目管理列表数据
	 * @author chen
	 */
	@Log("获取项目管理列表数据")
	@RequestMapping("list")
	@ResponseBody
	@RequiresPermissions("project:project:view")
	public Grid list(Project project, GridParam param) {
		return projectService.findPage(project, param);
	}



	@Log("获取项目管理列表数据")
	@RequestMapping("getSelect")
	@ResponseBody
	public List<Project> getSelect(Project project) {
		return projectService.findList(project);
	}

	@Log("获取项目详细信息")
	@RequestMapping("getProjectInfo")
	@ResponseBody
	public Project getProjectInfo(Project project) {
		return projectService.get(project.getId());
	}
	/**
	 * @Description：项目管理表单页面--新增/编辑
	 * @author chen
	 */
	@Log("打开项目管理表单页面--新增/编辑")
	@RequestMapping(value = "form")
	@RequiresPermissions("project:project:view")
	public String form(Project project, ModelMap model) {
		if (project.getId()!=null){
			project = projectService.get(project.getId());
			model.addAttribute("project", project);
			return "/project/editProjectForm";
		}else {
			return "/project/projectForm";
		}
	}
	/**
	 * @Description：项目管理新增/编辑 保存方法
	 * @author chen
	 */
	@Log("新增/编辑项目管理")
	@RequestMapping("saveProject")
	@RequiresPermissions("project:project:save")
	public String saveProject(Project project, ModelMap model){
		try{
			projectService.save(project);
		}catch(Exception e){
			log.error("保存失败！ msg={}", e.getMessage(), e);

			model.addAttribute("error", "保存失败！");
			return form(project, model);
		}
		return successPath;
	}
	/**
	 * @Description：项目管理数据删除方法
	 * @author chen
	 */
	@Log("删除项目管理")
	@RequestMapping("delProject")
	@ResponseBody
	@RequiresPermissions("project:project:del")
	public Json delProject(Project project){
		Json json = new Json();
		try{
			projectService.delete(project);
			json.setSuccess(true);
		}catch(Exception e){
			log.error("删除失败！ msg={}", e.getMessage(), e);

			json.setSuccess(false);
			json.setMsg("删除失败！");
		}
		return json;
	}

}