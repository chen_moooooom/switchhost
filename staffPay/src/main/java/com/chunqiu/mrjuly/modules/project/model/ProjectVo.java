package com.chunqiu.mrjuly.modules.project.model;

import lombok.Data;

@Data
public class ProjectVo {
    private String nature1;		// 属性1
    private String nature2;		// 属性2
    private String nature3;		// 属性3
    private String nature4;		// 属性4
    private String nature5;		// 属性5
    private String nature6;		// nature6
    private String nature7;		// nature7
    private String nature8;		// nature8
    private String nature9;		// nature9
    private String nature10;		// nature10
    private String nature11;		// nature11
    private String nature12;		// nature12
    private String nature13;		// nature13
    private String nature14;		// nature14
    private String nature15;		// nature15
    private String nature16;		// nature16
    private String nature17;		// nature17
    private String nature18;		// nature18
    private String nature19;		// nature19
    private String nature20;		// nature20
    private String nature21;		// nature21
    private String nature22;		// nature22
    private String nature23;		// nature23
    private String nature24;		// nature24
    private String nature25;		// nature25
}
