package com.chunqiu.mrjuly.modules.project.dao;

import com.chunqiu.mrjuly.common.persistence.CrudDao;
import com.chunqiu.mrjuly.modules.project.model.Project;

/**
 * 项目管理DAO接口
 * @author chen
 * @version 2019-12-03
 */
public interface ProjectDao extends CrudDao<Project, Integer> {
	
}