package com.chunqiu.mrjuly.modules.project.model;

import com.chunqiu.mrjuly.common.utils.excel.annotation.ExcelField;
import org.hibernate.validator.constraints.Length;

import com.chunqiu.mrjuly.common.persistence.DataEntity;

/**
 * 项目管理Entity
 * @author chen
 * @version 2019-12-03
 */
public class Project extends DataEntity<Project, Integer> {
	
	private static final long serialVersionUID = 1L;
	@ExcelField(title = "项目名称", type = 0, align = 2, sort = 0)
	private String projectName;		// 项目名称
	private Integer projectNum;		// 项目总数
	private String nature;		// 属性
	
	public Project() {
		super();
	}

	public Project(Integer id){
		super(id);
	}

	@Length(min=0, max=255, message="项目名称长度必须介于 0 和 255 之间")
	public String getProjectName() {
		return projectName;
	}

	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}
	
	public Integer getProjectNum() {
		return projectNum;
	}

	public void setProjectNum(Integer projectNum) {
		this.projectNum = projectNum;
	}
	
	@Length(min=0, max=255, message="属性长度必须介于 0 和 255 之间")
	public String getNature() {
		return nature;
	}

	public void setNature(String nature) {
		this.nature = nature;
	}
	
}