package com.chunqiu.mrjuly.modules.staff.controller;

import com.chunqiu.mrjuly.common.annotation.Log;
import com.chunqiu.mrjuly.common.persistence.BaseController;
import com.chunqiu.mrjuly.common.vo.Grid;
import com.chunqiu.mrjuly.common.vo.GridParam;
import com.chunqiu.mrjuly.common.vo.Json;
import com.chunqiu.mrjuly.modules.staff.model.Staff;
import com.chunqiu.mrjuly.modules.staff.service.StaffService;
import lombok.extern.slf4j.Slf4j;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * 职工管理Controller
 *
 * @author chen
 * @version 2019-12-02
 */
@Slf4j
@Controller
@RequestMapping(value = "${adminPath}/staff/staff")
public class StaffController extends BaseController {

    @Autowired
    private StaffService staffService;

    /**
     * @Description：职工管理列表页面
     * @author chen
     */
    @Log("打开职工管理列表页面")
    @RequestMapping("index")
    @RequiresPermissions("staff:staff:view")
    public String index() {
        return "/staff/staffList";
    }

    /**
     * @Description：职工管理列表数据
     * @author chen
     */
    @Log("获取职工管理列表数据")
    @RequestMapping("list")
    @ResponseBody
    @RequiresPermissions("staff:staff:view")
    public Grid list(Staff staff, GridParam param) {
        return staffService.findPage(staff, param);
    }

    /**
     * @Description：职工管理表单页面--新增/编辑
     * @author chen
     */
    @Log("打开职工管理表单页面--新增/编辑")
    @RequestMapping(value = "form")
    @RequiresPermissions("staff:staff:view")
    public String form(Staff staff, ModelMap model) {
        if (staff.getId()!=null){
            staff = staffService.get(staff.getId());
            model.addAttribute("staff", staff);
            return "/staff/staffForm";
        }else {
            return "/staff/addStaffForm";
        }

    }

    /**
     * @Description：职工管理新增/编辑 保存方法
     * @author chen
     */
    @Log("新增/编辑职工管理")
    @RequestMapping("saveStaff")
    @RequiresPermissions("staff:staff:save")
    public String saveStaff(Staff staff, ModelMap model) {
        try {
            staffService.save(staff);
        } catch (Exception e) {
            log.error("保存失败！ msg={}", e.getMessage(), e);

            model.addAttribute("error", "保存失败！");
            return form(staff, model);
        }
        return successPath;
    }

    /**
     * @Description：职工管理数据删除方法
     * @author chen
     */
    @Log("删除职工管理")
    @RequestMapping("delStaff")
    @ResponseBody
    @RequiresPermissions("staff:staff:del")
    public Json delStaff(Staff staff) {
        Json json = new Json();
        try {
            staffService.delete(staff);
            json.setSuccess(true);
        } catch (Exception e) {
            log.error("删除失败！ msg={}", e.getMessage(), e);

            json.setSuccess(false);
            json.setMsg("删除失败！");
        }
        return json;
    }

}