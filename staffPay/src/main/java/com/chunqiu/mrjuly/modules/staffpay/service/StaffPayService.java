package com.chunqiu.mrjuly.modules.staffpay.service;

import javax.annotation.Resource;

import com.chunqiu.mrjuly.common.persistence.CrudService;
import org.springframework.stereotype.Service;
import com.chunqiu.mrjuly.modules.staffpay.model.StaffPay;
import com.chunqiu.mrjuly.modules.staffpay.dao.StaffPayDao;

/**
 * 职工薪资管理Service
 * @author chen
 * @version 2019-12-03
 */
@Service
public class StaffPayService extends CrudService<StaffPayDao, StaffPay, Integer> {
	@Resource
	private StaffPayDao dao;

}